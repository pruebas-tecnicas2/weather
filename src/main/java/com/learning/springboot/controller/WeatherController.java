package com.learning.springboot.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learning.springboot.exception.WeatherNotFoundException;
import com.learning.springboot.model.Weather;
import com.learning.springboot.service.WeatherService;

@RestController
@RequestMapping(value = "/weather")
public class WeatherController {
	
	private static final Logger log = LoggerFactory.getLogger(WeatherController.class);
	
	@Autowired
	WeatherService weatherService;
	
	@GetMapping("{city}")
	public Weather getAPIRestWeather(@PathVariable("city") String city) {
		log.info("Método API Weather, pasando como parámetro:"+city);
		return weatherService.getWeather(city).orElseThrow(() -> new WeatherNotFoundException(city));
	}
}
