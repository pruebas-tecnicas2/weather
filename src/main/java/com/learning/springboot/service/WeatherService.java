package com.learning.springboot.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.learning.springboot.model.Weather;

@Service
public class WeatherService {
	
	@Value("${endpoint.werApp}")
	private String weather_APIRest;
	
	public Optional<Weather> getWeather(String city){
		String url = String.format(weather_APIRest, city);
		WebClient webClient = WebClient.create(url);
		Weather weather = null;
		Optional<Object> ob;
		try {
			ob = Optional.of(webClient.get().uri(url).retrieve().bodyToMono(Object.class).block());
			
		} catch (Exception e) {
			return Optional.ofNullable(weather);
		}
		if(ob.isPresent()) {
			ObjectMapper obMapper = new ObjectMapper();
			try {
				String weatherJSON = obMapper.writeValueAsString(ob.orElse(""));
				ObjectNode obNode = new ObjectMapper().readValue(weatherJSON, ObjectNode.class);
				String temp = obNode.get("current_condition").get(0).get("temp_C").asText();
				String areaNameValue = obNode.get("nearest_area").get(0).get("areaName").get(0).get("value").asText();
				weather = new Weather(areaNameValue, temp);
			} catch (JsonMappingException e) {
				e.printStackTrace();
			}catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}		
		return Optional.ofNullable(weather);
	}
}
