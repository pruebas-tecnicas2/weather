package com.learning.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NO_CONTENT)
public class WeatherNotFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	public WeatherNotFoundException(String city) {
		super("city introduced: "+city+ " not found");
	}
}
