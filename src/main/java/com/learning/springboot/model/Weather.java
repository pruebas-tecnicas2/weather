package com.learning.springboot.model;

//@Entity se indica cuando se quiere persistencia de datos
//@Table(name ="weather")
public class Weather {	
	
	private String city;
	private String temp;
	
	public Weather() {
		super();
	}

	public Weather(String city, String temp) {
		super();
		this.city = city;
		this.temp = temp;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}
	
}
