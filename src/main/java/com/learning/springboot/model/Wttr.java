package com.learning.springboot.model;

import java.util.List;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Wttr {

	private List<CurrentCondition> currentCondition;
	private List<NearestArea> nearestArea;

	public Wttr() {
		super();
	}

	public List<CurrentCondition> getCurrentCondition() {
		return currentCondition;
	}

	public void setCurrentCondition(List<CurrentCondition> currentCondition) {
		this.currentCondition = currentCondition;
	}

	public List<NearestArea> getNearestArea() {
		return nearestArea;
	}

	public void setNearestArea(List<NearestArea> nearestArea) {
		this.nearestArea = nearestArea;
	}

}