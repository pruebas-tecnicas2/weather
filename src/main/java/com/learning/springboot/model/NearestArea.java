package com.learning.springboot.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class NearestArea {

	private List<AreaName> areaName;

	public NearestArea() {
		super();
	}

	public List<AreaName> getAreaName() {
		return areaName;
	}

	public void setAreaName(List<AreaName> areaName) {
		this.areaName = areaName;
	}
}
