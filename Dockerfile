FROM maven:3.8.3-openjdk-17 AS build   			
COPY src /build/app/src      			
COPY pom.xml /build/app
RUN mvn -f /build/app/pom.xml clean package

FROM openjdk:17-ea-15-jdk-slim
ARG JAR_FILE=target/Weather-0.0.1-SNAPSHOT.jar
COPY --from=build /build/app/target/Weather-0.0.1-SNAPSHOT.jar /usr/local/lib/weather.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=local", "/usr/local/lib/weather.jar"]